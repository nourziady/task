function rec(array,next,final) {
  var index = 0;
  nextIter(array[index]);
  function nextIter(item) {
    if (index < array.length) {
      setTimeout(function () {
        next(array[index],function functionName() {
          index ++;
          nextIter(array[index]);
        },index);
      }, 0);
    }
    else {
      final(index);
    }
  }
}


module.exports = rec;
