let request = require("request");
var redis = require("redis");
var client = redis.createClient();
let async = require("./asyncrec");
let config = require("./config.json");

client.on("error", function(err) {
  console.log("Error " + err);
});

let omApiKey = config.omdbapikey;
let searchQuery = "marvel";
let year = 2018;
let searchURL = `http://www.omdbapi.com/?apikey=${omApiKey}&s=${searchQuery}&y=${year}`;
let dataURL = `http://www.omdbapi.com/?apikey=${omApiKey}&i=`;

let init = cb => {
  client.flushall()
  let movies = [];
  let reqSearch = (page, reqcb) => {
    let newSearchURL = searchURL;
    if (page) {
      newSearchURL += `&page=${page}`;
    }
    request(newSearchURL, (err, resp, body) => {
      body = JSON.parse(body);
      for (let i = 0; i < body.Search.length; i++) {
        const movie = body.Search[i];
        movies.push(movie);
      }
      if (body.totalResults > 10 && !page) {
        let pages = Math.ceil(body.totalResults / 10);
        let pagesArray = Array.from(Array(pages).keys());
        pagesArray.shift();
        async(
          pagesArray,
          (movie, next) => {
            reqSearch(movie + 1, () => {
              next();
            });
          },
          final => {
            reqcb();
          }
        );
      } else {
        reqcb();
      }
    });
  };

  reqSearch(false, () => {
    client.keys("*", (err, keys) => {
      if (keys.length == 0) {
        async(
          movies,
          (movie, next) => {
            request(dataURL + movie.imdbID, (err, res, body) => {
              body = JSON.parse(body);
              movie.data = body;
              client.set(movie.imdbID, JSON.stringify(movie), () => {
                next();
              });
            });
          },
          () => {
            cb(movies);
          }
        );
      } else {
        cb({});
      }
    });
  });
};

module.exports = init;
