let express = require("express");
let app = express();
let init = require("./init");
let redis = require("redis").createClient();
let async = require("./asyncrec");
app.set("view engine", "ejs");
app.use(express.static("public"));

redis.on("error", err => {
  console.log(err);
});

app.get("/", (req, res) => {
  console.log("hi");

  let movies = [];
  redis.keys("*", (err, keys) => {
    async(
      keys,
      (id, next) => {
        redis.get(id, (err, movie) => {
          movies.push(JSON.parse(movie));
          next();
        });
      },
      () => {
        res.render("index.ejs", { movies });
      }
    );
  });
});

app.get("/:id", (req, res) => {
  redis.get(req.params.id, (err, movie) => {
    console.log(typeof movie);
    console.log(movie);
    res.render("movie.ejs", { movie: JSON.parse(movie) });
  });
});

init(movies => {
  app.listen(8080);
});
